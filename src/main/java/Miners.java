import blockChainLibrary.BlockChain;
import tasks.MinerTask;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Miners {
    public static void main(String[] args){
        var blockChain = new BlockChain();
        var tasks = Arrays.asList(
                new MinerTask("Miner 1", blockChain),
                new MinerTask("Miner 2", blockChain),
                new MinerTask("Miner 3", blockChain),
                new MinerTask("Miner 4", blockChain));

        ExecutorService executorService = Executors.newFixedThreadPool(tasks.size());
        tasks.forEach(executorService::execute);
    }
}
