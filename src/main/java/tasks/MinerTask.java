package tasks;

import lombok.extern.slf4j.Slf4j;
import blockChainLibrary.Block;
import blockChainLibrary.BlockChain;
import java.util.Date;

@Slf4j
public class MinerTask implements Runnable {
    private final String minerName;
    private final BlockChain blockChain;

    public MinerTask(String taskName, BlockChain blockChain) {
        this.minerName = taskName;
        this.blockChain = blockChain;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            var previousHash = blockChain.getLastHash();
            var newBlock = new Block(
                    "This is a New Block from " + this.minerName,
                    previousHash,
                    new Date().getTime());

            this.mineBlock(blockChain.getPrefix(), newBlock);
            blockChain.addBlock(newBlock, minerName);
        }
    }

    public String mineBlock(int prefix, Block newBlock) {
        var prefixString = blockChain.getPrefixString();
        while (!newBlock.getHash().substring(0, prefix).equals(prefixString)
                && blockChain.getLastHash() == newBlock.getPreviousHash()) {
            newBlock.setNonce(newBlock.getNonce()+1);
            newBlock.setHash(newBlock.calculateBlockHash());
        }
        return newBlock.getHash();
    }
}
